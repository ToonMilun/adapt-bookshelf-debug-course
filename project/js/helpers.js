define([
	"core/js/adapt"
  ], function(Adapt) {

	/**
	 * Define helpers specific to this project here.
	 * Be SURE to prefix them all with "c-".
	 */

	var helpers = {

        "c-example": function() {

            let context = arguments[arguments.length-1];
			let content = context.fn(this); // Get the contents of the helper.

            let html = Handlebars.templates["c-example"](_.extend(
				context.hash,
				{
					content: content
				}
			));
            return new Handlebars.SafeString(html);
		}
	}

	for (var name in helpers) {
        if (helpers.hasOwnProperty(name)) {
            Handlebars.registerHelper(name, helpers[name]);
        }
    }
});